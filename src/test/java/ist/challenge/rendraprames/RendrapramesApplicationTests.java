package ist.challenge.rendraprames;

import ist.challenge.rendraprames.dto.UsersRequestDTO;
import ist.challenge.rendraprames.dto.UsersResponseDTO;
import ist.challenge.rendraprames.exception.ResourceAlreadyExistException;
import ist.challenge.rendraprames.model.Users;
import ist.challenge.rendraprames.repo.UsersRepo;
import ist.challenge.rendraprames.response.ResponseHandler;
import ist.challenge.rendraprames.service.implement.UsersServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class RendrapramesApplicationTests {

	@Mock
	private UsersRepo usersRepo;

	@InjectMocks
	private UsersServiceImpl userService;

	private Users users;

	@BeforeEach
	public void setup(){

		users = Users.builder()
				.userId(1L)
				.username("rendrap")
				.password("123456")
				.build();
	}

	@DisplayName("Unit Test 1")
	@Test
	public void getAllUserPositiveTest(){
		Users user1 = Users.builder()
				.userId(1L)
				.username("rendra")
				.password("123456")
				.build();
		List<UsersResponseDTO> userResponsesList = new ArrayList<>();
		users.setUsername(user1.getUsername());
		users.setPassword(user1.getPassword());
		UsersResponseDTO userResponseDTO = users.convertToResponse();
		userResponsesList.add(userResponseDTO);
		given(usersRepo.findAll()).willReturn(List.of(users));
		ResponseEntity<Object> userList = userService.getAllUsers();
		assertThat(userList).isNotNull();
		assertThat(userResponsesList.size()).isEqualTo(1);
	}

	@DisplayName("Unit Test 2")
	@Test
	public void signUpPositiveTest(){
		UsersRequestDTO request = new UsersRequestDTO();
		request.setUsername("aditya");
		request.setPassword("1234555");
		ResponseEntity<Object> savedUser = userService.signUp(request);
		assertThat(savedUser).isNotNull();
	}

}
