package ist.challenge.rendraprames.model;

import ist.challenge.rendraprames.dto.UsersResponseDTO;
import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "users")
@Data
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    @Column(name="username", nullable = false, unique = true, length = 25)
    private String username;

    @Column(name="password", nullable = false, length = 25)
    private String password;

    public UsersResponseDTO convertToResponse(){
        return UsersResponseDTO.builder()
                .userId(this.userId)
                .username(this.username)
                .build();
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}