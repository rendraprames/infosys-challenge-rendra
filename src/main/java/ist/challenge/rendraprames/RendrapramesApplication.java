package ist.challenge.rendraprames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RendrapramesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RendrapramesApplication.class, args);
	}

}
