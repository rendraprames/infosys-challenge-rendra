package ist.challenge.rendraprames.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersResponseDTO {
    private Long userId;
    private String username;

    @Override
    public String toString() {
        return "UsersResponseDTO{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}
