package ist.challenge.rendraprames.dto;

import ist.challenge.rendraprames.model.Users;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersRequestDTO {
    private String username;
    private String password;
}
