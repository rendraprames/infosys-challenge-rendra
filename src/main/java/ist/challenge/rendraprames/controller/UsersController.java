package ist.challenge.rendraprames.controller;

import ist.challenge.rendraprames.dto.UsersRequestDTO;
import ist.challenge.rendraprames.service.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/challenge/IST")
public class UsersController {
    UsersService usersService;

    /*
     * Get All Users
     * @return usersService.getAllUsers();
     */
    @GetMapping("user/retrieveAll")
    public ResponseEntity <Object> getAll(){
        return usersService.getAllUsers();
    }

    /**
     * Create New User
     * @param usersRequestDTO input request
     * @return usersService.signUp();
     */
    @PostMapping("user/SignUp")
    public ResponseEntity<Object> signUpUser(@RequestBody UsersRequestDTO usersRequestDTO){
        return usersService.signUp(usersRequestDTO);
    }

    /**
     * Login Existing User
     * @param usersRequestDTO input request
     * @return usersService.loginUser();
     */
    @PostMapping("user/login")
    public ResponseEntity<Object> loginUser(@RequestBody UsersRequestDTO usersRequestDTO){
        return usersService.loginUser(usersRequestDTO);
    }

    /**
     * Update User Data
     * @param userId Path Variable request
     * @param usersRequestDTO input data request
     * @return usersService.updateUser();
     */
    @PutMapping("user/update/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable Long userId, @RequestBody UsersRequestDTO usersRequestDTO){
        return usersService.updateUser(userId, usersRequestDTO);
    }
}

