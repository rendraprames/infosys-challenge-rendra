package ist.challenge.rendraprames.service;

import ist.challenge.rendraprames.dto.UsersRequestDTO;
import ist.challenge.rendraprames.model.Users;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UsersService {
    ResponseEntity<Object> getAllUsers();
    ResponseEntity<Object> signUp(UsersRequestDTO usersRequestDTO);
    ResponseEntity<Object> loginUser(UsersRequestDTO usersRequestDTO);
    ResponseEntity<Object> updateUser(Long UserId, UsersRequestDTO usersRequestDTO);

}