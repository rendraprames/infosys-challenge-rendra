package ist.challenge.rendraprames.service.implement;

import ist.challenge.rendraprames.dto.UsersRequestDTO;
import ist.challenge.rendraprames.dto.UsersResponseDTO;
import ist.challenge.rendraprames.exception.ResourceAlreadyExistException;
import ist.challenge.rendraprames.exception.ResourceNotFoundException;
import ist.challenge.rendraprames.model.Users;
import ist.challenge.rendraprames.repo.UsersRepo;
import ist.challenge.rendraprames.response.ResponseHandler;
import ist.challenge.rendraprames.service.UsersService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import java.util.*;


@Service
@AllArgsConstructor
@Slf4j
public class UsersServiceImpl implements UsersService {
    private final UsersRepo usersRepo;
    private static final Logger logger = LogManager.getLogger(UsersServiceImpl.class);

    /**
     * Get All User
     * @return
     */
    @Override
    public ResponseEntity<Object> getAllUsers() {
        try {
            List<Users> users = usersRepo.findAll();
            if(users.isEmpty()){
                throw new ResourceNotFoundException("Data Not Found");
            }
            List<UsersResponseDTO> usersResponseList = new ArrayList<>();
            logger.info("==================== Logger Start Get All User ====================");
            for (Users dataResult : users){
                logger.info("ID Akun      : " + dataResult.getUserId());
                logger.info("Username     : " + dataResult.getUsername());;
                logger.info("Password     : " + dataResult.getPassword());
                logger.error("------------------------------------");
                UsersResponseDTO usersResponseDTO = dataResult.convertToResponse();
                usersResponseList.add(usersResponseDTO);
            }
            logger.info("==================== Logger End Get All User ====================");
            return ResponseHandler.generateResponse("Successfully Get All User!", HttpStatus.OK, usersResponseList);
        } catch (Exception e) {
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Bad Request!!");
        }
    }

    /**
     * Create New User
     * @param usersRequestDTO
     * @return
     */
    @Override
    public ResponseEntity<Object> signUp(UsersRequestDTO usersRequestDTO) {
        try{
            if(usersRequestDTO.getUsername().isEmpty() || usersRequestDTO.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username dan / atau password kosong");
            }

            if(usersRepo.existsByUsername(usersRequestDTO.getUsername())){
                return ResourceAlreadyExistException.existsException(HttpStatus.CONFLICT, "Username Sudah Terpakai");
            }

            Users users = new Users();
            users.setUsername(usersRequestDTO.getUsername());
            users.setPassword(usersRequestDTO.getPassword());
            usersRepo.save(users);
            logger.info("==================== Logger Start Sign Up User ====================");
            logger.info("ID Akun      : " + users.getUserId());
            logger.info("Username     : " + users.getUsername());;
            logger.info("Password     : " + users.getPassword());
            UsersResponseDTO usersResponseDTO = users.convertToResponse();
            logger.info("==================== Logger End Get Sign Up User ====================");
            return ResponseHandler.generateResponse("Successfully Create User", HttpStatus.CREATED, usersResponseDTO);
        } catch (Exception e){
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Failed Create User");
        }
    }

    /**
     * Login Existing User
     * @param usersRequestDTO
     * @return
     */
    @Override
    public ResponseEntity<Object> loginUser(UsersRequestDTO usersRequestDTO) {
        try{
            if(usersRequestDTO.getUsername().isEmpty() || usersRequestDTO.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username dan / atau password kosong");
            }

            if(!usersRepo.existsByUsername(usersRequestDTO.getUsername())){
                return ResourceAlreadyExistException.existsException(HttpStatus.FORBIDDEN, "Username Tidak Ditemukan");
            }

            Optional<Users> users = usersRepo.findByUsername(usersRequestDTO.getUsername());
            if(!usersRequestDTO.getPassword().equals(users.get().getPassword())){
                return ResourceAlreadyExistException.existsException(HttpStatus.FORBIDDEN, "Password Salah");
            }
            logger.info("==================== Logger Start Login User ====================");
            logger.info("Username     : " + usersRequestDTO.getUsername());;
            logger.info("Password     : " + usersRequestDTO.getPassword());
            logger.info("==================== Logger End Get Login User ====================");
            return ResponseHandler.successResponse("Login Success", HttpStatus.OK);
        }catch (Exception e){
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Login Failed");
        }
    }

    /**
     * Update User Data
     * @param UserId
     * @param usersRequestDTO
     * @return
     */
    @Override
    public ResponseEntity<Object> updateUser(Long UserId, UsersRequestDTO usersRequestDTO) {
        try{
            if(usersRequestDTO.getUsername().isEmpty() || usersRequestDTO.getPassword().isEmpty()){
                throw new ResourceNotFoundException("Username dan / atau password kosong");
            }
            Users users = usersRepo.getById(UserId);
            if(usersRepo.existsByUsername(usersRequestDTO.getUsername())){
                return ResourceAlreadyExistException.existsException(HttpStatus.CONFLICT, "Username Sudah Terpakai");
            }

            if(usersRequestDTO.getPassword().equals(users.getPassword())){
                return ResourceAlreadyExistException.existsException(HttpStatus.BAD_REQUEST, "Password Tidak Boleh Sama Dengan\n" +
                        "Password Sebelumnya");
            }

            users.setUserId(UserId);
            users.setUsername(usersRequestDTO.getUsername());
            users.setPassword(usersRequestDTO.getPassword());
            usersRepo.save(users);
            UsersResponseDTO usersResponseDTO = users.convertToResponse();
            logger.info("==================== Logger Start Update User ====================");
            logger.info("ID Akun      : " + usersResponseDTO.getUserId());
            logger.info("Username     : " + usersResponseDTO.getUsername());;
            logger.info("Password     : " + usersRequestDTO.getPassword());
            logger.info("==================== Logger End Get Update User ====================");
            return ResponseHandler.generateResponse("Successfully Update Data", HttpStatus.CREATED,usersResponseDTO);
        }catch (Exception e){
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Failed Update Data");
        }
    }

}
